import React, {Component} from 'react'
import { View,StyleSheet,Text,Image,ScrollView  } from 'react-native'
import Axios from 'axios' 
class Assegiment extends  Component{

    constructor(props) {
      super(props);
      this.state = {
        tampungan: {},
        daftarFilm: []
      }
    }

    fetchAPI = async()=>{
      const tampungan2 = await Axios.get ('http://www.omdbapi.com/?s=naruto&apikey=997061b4&')
      const tampungan3 = tampungan2.data

      this.setState({
        tampungan: tampungan2,
        daftarFilm: tampungan3.Search
      })
    }

    componentDidMount(){
      this.fetchAPI()
    }

  render(){
    return(
      <View style={styles.container}>
        <View style={styles.header}>
          <Text >
            Daftar Film
          </Text>
        </View>
        <ScrollView> 
        <View>
          {
            this.state.daftarFilm.map((item,i)=>{
              return(
              <View style={styles.Movie} key={i}>
                
                <Image source={{uri:item.Poster}} style={styles.Image} />
                <View>
                  <Text style={{color:'lightblue'}}>
                    {item.Title}
                  </Text> 
                  <Text style={{color:'lightblue'}}>
                    {item.Type}
                  </Text>
                </View>
              </View>
              
              )
            })
          }
          
        </View>
        </ScrollView>
      </View>
    )
  }
}
export default Assegiment;

const styles = StyleSheet.create({

   container:{
      backgroundColor:'lightblue',
      flex:1

    },
    header:{
      marginVertical:15,
      alignItems:'center'
    },
    Movie:{
      backgroundColor:'darkgreen',
      flexDirection:'row',
      padding:15,
      height:'15%',
      marginHorizontal:20,
      borderRadius:20,
      marginBottom:10
        
    },
    Image :{
      width:'20%',
      height:'100%',
      marginRight:10
    }
  })